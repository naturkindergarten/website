<?php

$url = urldecode(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
if ('/' !== $url && file_exists("${_SERVER['DOCUMENT_ROOT']}{$url}")) {
    return false;
}

/** @noinspection PhpIncludeInspection */
require_once "${_SERVER['DOCUMENT_ROOT']}/index.php";
