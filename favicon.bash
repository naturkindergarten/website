#!/usr/bin/env bash

# TODO improve coding style

SRC=$1
TARGET=public

convert $SRC -resize 256x256 "$TARGET/favicon-256.png"
convert $SRC -define icon:auto-resize=16,32,48,64,152,196 -compress zip "$TARGET/favicon.ico"
cp $SRC $TARGET
