---
title: Der Verein
description:
show_cta: true
---
# Der Verein

Wir sind ein gemeinnütziger Verein, der sich im Januar 2020 gegründet hat, und seit August 2020 einen Naturkindergarten für Kinder von 3 bis 6 Jahren in Eigenverantwortung betreibt.
Der Verein trägt sich durch möglichst viele Mitglieder. Die Mitgliedschaft bietet die Möglichkeit, den Kindergarten aktiv mitzugestalten.

Ziel der Vereinstätigkeit ist es, einen Kindergarten zu betreiben, der Kindern vielfältige Naturerfahrungen ermöglicht und sie auf ihrem Weg zu eigenverantwortlich handelnden, selbst- und verantwortungsbewussten Persönlichkeiten unterstützt.

Weiterhin sehen wir uns als Teil des dörflichen Lebens und legen großen Wert auf eine gute, generationenübergreifende Vernetzung, z.B. mit der Dorfschmiede und der Grundschule Freienseen.

Der Verein wird vertreten durch:

- Meike Engeln, 1. Vorsitzende
- Ester Keiner, 2. Vorsitzende
- Matthias Meyer, Schatzmeister
- Carolin Sauerborn, Schriftführerin

sowie zehn BeisitzerInnen
