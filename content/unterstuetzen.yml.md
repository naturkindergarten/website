---
title: Deine Unterstützung
description:
show_cta: true
---
# Deine Unterstützung

neben der Mitgliedschaft in unserem Verein bitten wir auch um Spenden. Denn es gibt vieles, was es anzuschaffen gilt:

- Tipi
- Werkzeug
- Spiel- und Bastelmaterial
- Gartengeräte
- Bücher
- …

Wir würden uns freuen, wenn wir dich als Mitglied oder UnterstützerIn für unseren Verein gewinnen können und bedanken uns herzlich im Namen unserer Kinder.

