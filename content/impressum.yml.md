---
title: Impressum
description:
is_template: true
---
# Impressum
## Angaben gemäß § 5 TMG
    
{{ contact()|raw }}

### Vertretungsberechtigter Vorstand

Meike Engeln, Ester Keiner

### Inhaltlich Verantwortliche gemäß § 55 Abs. 2 RStV

Meike Engeln

## Plattform der EU-Kommission zur Online-Streitbeilegung

[https://webgate.ec.europa.eu/odr/main/](https://webgate.ec.europa.eu/odr/main/)

## Layout und Entwicklung der Website

[Layout und Entwicklung der Website: <b>macrominds</b> (Inh. Thomas Praxl)](https://www.macrominds.de)</a>.
