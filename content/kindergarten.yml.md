---
title: Der Kindergarten
description:
show_cta: true
---
# Der Kindergarten

- Wiesengrundstück mit Bauwagen und Permakulturgarten in Freienseen Richtung Grillhütte
- eine Gruppe mit max. 25 Kindern ab 3 Jahren
- Betreuungszeit von 7:30 bis 15:30 Uhr
- qualifizierte Betreuung der Kinder durch drei staatlich geprüfte ErzieherInnen mit zum Teil langjähriger naturpädagogischer Erfahrung und Freinet-Ausbildung 
- Mittagessen von BigEavens Catering
- das Mittagessen wird am Platz eingenommen
- die Nachmittagsbetreuung findet ab 14 Uhr im Blauen Haus der Grundschule Freienseen statt
