---
title: Naturkindergarten Seenbachtal e.V.
description: 
show_cta: true
---
# Naturkindergarten Seenbachtal e.V.

## Aktuelles

Liebe Freunde und Unterstützer,  
liebe Interessierte,

wir haben es geschafft und sind sehr froh: 
seit dem 01. August 2020 werden in unserem Naturkindergarten 23 Kinder von zwei Erzieherinnen und einem Erzieher betreut und schon bald ist die Gruppe mit zwei weiteren Kindern voll ausgelastet. 
Mit viel Herzblut, großem Arbeitseinsatz von Eltern, Mitarbeitern, Vereinsmitgliedern und sonstigen Unterstützern sowie einer großen Anzahl von Spendern ist es gelungen!
 
Trotz Corona, etlicher Hürden und Widerstände und oft großem Bangen, ob es denn überhaupt gelingt…

Ein wirklich schöner Platz für Alle ist entstanden. Unser Container, der als Wetterschutz für die Kinder und ErzieherInnen dient, hat eine neue Gasheizung erhalten und ist um eine großzügig angelegte Veranda erweitert worden. Eine angrenzende Gartenhütte dient als Materiallager. Ein großer Sandspielbereich wurde geschaffen und erste Pflanzaktionen wurden getätigt.  Die ersten Wochen zeigen, dass sich Kinder, ErzieherInnen und Eltern wohlfühlen und sich freuen, dass unser Projekt verwirklicht werden konnte.

Trotzdem sind wir auch weiterhin auf eure Unterstützung angewiesen und freuen uns über jede und jeden, der hierzu etwas beiträgt.  

Für eure Kinder – für unsere Kinder!  
