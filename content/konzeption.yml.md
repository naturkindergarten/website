---
title: Die Konzeption
description:
show_cta: true
---
# Die Konzeption

Die Natur ist in unseren Augen die optimale Umgebung für selbstbestimmtes, kreatives und sinnerfülltes Spiel.

Die Kinder können ihre Persönlichkeit frei entfalten. Die Tätigkeiten und Projekte können freiwillig und nach eigenem Interesse ausgesucht werden.

Durch tastende Versuche und entdeckendes Lernen ist das Kind aktiver Gestalter seiner Lebenswelt.

Die Kinder nehmen die Natur in ihrem Jahresverlauf unmittelbar mit allen Sinnen wahr und fühlen sich mit der Natur verbunden, was Grundlage für zukünftiges umweltbewusstes und nachhaltiges Verhalten ist.

Um den Kindern die Möglichkeit zu geben auch über den grünen Tellerrand zu blicken, erkunden wir die Welt auch außerhalb Freienseens.
