<?php

use Macrominds\App;
use Macrominds\DefaultConfig;
use Macrominds\Frontmatter\FrontmatterServiceProvider;
use Macrominds\MarkdownServiceProvider;
use Macrominds\TwigServiceProvider;
use Macrominds\YamlParserServiceProvider;

require_once __DIR__ . '/../vendor/autoload.php'; // @codeCoverageIgnore

$app = new App(realpath(__DIR__ . '/..'));

$configuration = (new DefaultConfig())->merge([
    'variables.var.locale' => 'en',
    'template-engine' => [
        'debug-mode' => false,
        'exported'   => [
            'functions' => [
                'css' => function (string $name): string {
                    return "/css/{$name}";
                },
                'contact' => function($heading=null): string {
                    return contact($heading);
                },
                'donate' => function($heading=null): string {
                    return donate($heading);
                }
            ],
        ],
    ],
]);

$app->configure($configuration);

$app->registerServiceProviders([
    new FrontmatterServiceProvider($configuration),
    new YamlParserServiceProvider($configuration),
    new MarkdownServiceProvider($configuration),
    new TwigServiceProvider($configuration),
]);

$app->run();

function contact($heading) {
    return heading ($heading) . <<<HTML
<p>
NaturKINDERgarten Seenbachtal e.V.</p>
<p>
1. Vorsitzende<br>
Meike Engeln<br>
Tunnelstraße 16<br>
35321 Laubach-Freienseen
</p>
<a href="tel:+4917697642205">0176-97642205</a>
<a href="mailto:info@naturkindergarten-seenbachtal.de">info@naturkindergarten-seenbachtal.de</a>
HTML;
}

function donate($heading) {
return heading($heading) . <<<HTML
<div class="donate-box">
    <i class="fa fa-credit-card"></i>
    <p>
        Volksbank Mittelhessen<br>
        IBAN DE12 5139 0000 0074 8502 00<br>
        BIC VBMHDE5F
    </p>
</div>
HTML;
}

function heading(string $heading = null) {
    if (empty($heading)) {
        return '';
    }
    return "<h2>{$heading}</h2>";
}
