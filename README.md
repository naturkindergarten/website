# Website Naturkindergarten Seenbachtal

## Entwicklungs- / oder Testsystem aufsetzen

Nach dem ersten clonen des Repositories bitte [.env.example](.env.example) nach `.env` kopieren und die Einstellungen an das System anpassen. Für ein Testsystem ist `APP_ENV=local` sinnvoll. Damit werden auf Produktivsystemen ausgeblendete Informationen auf Fehlerseiten angezeigt. Die `.env` Datei wird von git ignoriert.

Dann im Terminal `composer install` ausführen.

### Testserver starten

```bash
$ php -S localhost:8080 -t ./public ./router.php
```

Danach kann die Website im Browser unter [http://localhost:8080](http://localhost:8080) betrachtet werden. Damit sie von externen Geräten aus erreichbar ist, bitte in bash und Browser statt localhost die IP des Rechners verwenden.

## Tools

### Favicon und Logo erstellen

Das Favicon wird aus [assets/images/favicon/favicon.png](assets/images/favicon/favicon.png) erstellt, indem Du `composer make-favicon` aufrufst. Ich habe beschlossen, es dennoch zu versionieren, so dass das nur bei einer Änderung notwendig wird.

Das Logo wird automatisch skaliert, optimiert und in jpg + webp konvertiert mittels `composer make-logo`. Auch hier habe ich beschlossen, das Ergebnis zu versionieren. Auch das ist also nur bei einer Änderung notwendig.

## Deployment (en)

### Server system requirements

You need php 7.3 or php 7.4 and mod_rewrite. 
Composer, git and ssh access are required for auto deployment.

### Provisioning tools 

Use [the Uberspace Provisioning tool](https://gitlab.com/naturkindergarten/provision-uberspace) to setup or modify your Uberspace server.

### Auto deployment

We're using [Deployer](https://deployer.org/) for auto deployment.

#### Preparations

Edit [hosts.yml](hosts.yml) to match your git `repository` and `application` name. 

The keys `kiga-testing` and `kiga` are server names. Either configure your ssh servers (~/.ssh/config) to match
the names or rename the keys to your server(s). E.g. replace `kiga` with `my-website.com`. Of course,
you will still need to configure ssh for `naturkindergarten-seenbachtal.de`.

### Run the auto deployment

Run `vendor/bin/dep deploy {server-name}`, and replace `{server-name}` by one of your 
configured servers. 

E.g. run `vendor/bin/dep deploy kiga` if you haven't changed the name of it in 
[hosts.yml](hosts.yml).

### Special settings

#### Redirect to canonical URL

The .htaccess will redirect to https://www.naturkindergarten-seenbachtal.de if any other url has been requested. This is meant to prevent duplicate urls (search engines would 
inflict a penalty).

You can prevent it from doing so by placing a `dev` file as a sibling to your docroot. E.g. `cd /var/www/virtual/kiga/html && touch ../dev`. This signals that we're working on a development server and will turn off these kinds of redirects.

#### htaccess protection

You can htaccess protect the site by providing a valid `/var/www/virtual/kiga/.htuser` file. To do so for a user exampleuser, call `htpasswd -m -c /var/www/virtual/kiga/.htuser exampleuser` and set a password.

Then you can turn the protection on by providing an empty file protect as a sibling to html: `cd /var/www/virtual/kiga/html && touch ../protect`.

Turn off the protection by deleting the file `protect`.

## Troubleshooting

### Template cache is not invalidated

Set `template-engine.debug-mode` to `true` ([index.php](index.php)). This enables auto reload.
As a last resort, you can always simply `rm -r cache`.
